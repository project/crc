<?php

/**
 * @file
 * Configures the Create related content page.
 */

/**
 * Menu callback. Used to enable the module on the specified content
 * type by specifying a default preset to use.
 */
function crc_default_presets_form() {
  $form = array();

  $options = array(0 => t('Disabled'));
  $q = db_query("SELECT pid, name FROM {crc_preset}");
  while ($r = db_fetch_array($q)) {
    $options[$r['pid']] = t($r['name']);
  }

  $form['crc_settings'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#title' => t('Default template'),
    '#default_value' => (int) variable_get('crc_settings', 0),
  );
  $form['crc_settings']['#options'][0] = t('None');
  $form['crc_template'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('crc_template', 0),
    '#title' => t('Use for all standard "%cc" forms.', array('%cc' => t('Create content'))),
    '#description' => t('This directly modifies the standard Drupal %cc form for all users. These settings can be assigned to individual content types below.', array('%cc' => t('Create content'))),
  );

  $form['ct_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content type specific settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $types = node_get_types('names');
  foreach ($types as $type => $name) {
    $form['ct_settings']['crc_settings_'. $type] = array(
      '#type' => 'select',
      '#options' => $options,
      '#title' => t('Default template for !name', array('!name' => check_plain($name))),
      '#default_value' => (int) variable_get('crc_settings_'. $type, 0),
    );
    $form['ct_settings']['crc_template_'. $type] = array(
      '#type' => 'checkbox',
      '#default_value' => variable_get('crc_template_'. $type, 0),
      '#title' => t('Use for all !name "%cc" forms.', array('%cc' => t('Create content'), '!name' => check_plain($name))),
    );
  }
  return system_settings_form($form);
}

/**
 * Preset Admin callbacks and required functions.
 */
function crc_presets_list() {
  $header = array(t('Template'), t('Usage'), t('Actions'));
  $rows = array();

  $preset_usage = crc_preset_usage();

  // May have to consider pagination
  $query = db_query("SELECT * FROM {crc_preset} ORDER BY name");
  while ($tag = db_fetch_array($query)) {
    $row = array();
    $row[] = l($tag['name'], 'admin/settings/crc/'. $tag['pid']);
    $row[] = isset($preset_usage[$tag['pid']]) ? implode(', ', $preset_usage[$tag['pid']]) : '';
    $links = array();
    $links[] = l(t('Edit'), 'admin/settings/crc/'. $tag['pid']);
    $links[] = l(t('Delete'), 'admin/settings/crc/'. $tag['pid'] .'/delete');
    $row[] = implode('&nbsp;&nbsp;&nbsp;&nbsp;', $links);
    $rows[] = $row;
  }
  $output = theme('table', $header, $rows);
  return $output;
}

/**
 * Parses the variables table to discover
 * all presets that are in use.
 */
function crc_preset_usage() {
  $types = node_get_types('names');
  $presets = array();
  foreach ($types as $type => $name) {
    $pid = variable_get('crc_settings_'. $type, 0);
    if (!isset($presets[$pid])) {
      $presets[$pid] = array();
    }
    $presets[$pid][$type] = $name;
  }
  $pid = variable_get('crc_settings', 0);
  if (!isset($presets[$pid])) {
    $presets[$pid] = array();
  }
  // space avoids name clashes
  $presets[$pid]['system preset'] = t('System default');
  return $presets;
}

function crc_node_autocomplete($string = '') {
  $matches = array();
  if ($string) {
    $result = db_query_range("SELECT nid, title FROM {node} WHERE LOWER(title) LIKE LOWER('%s%%')", $string, 0, 10);
    while ($n = db_fetch_object($result)) {
      $matches[$n->nid .':'. $n->title] = check_plain($n->title);
    }
  }
  drupal_json($matches);
}

function crc_presets_form($form_state, $edit = array()) {
  drupal_add_js(drupal_get_path('module', 'crc') .'/crc.js');
  $edit += array(
    'pid' => '',
    'name' => '',
    'data' => array(),
  );
  $settings = crc_system_settings($edit['pid']);
  $form = array();
  $form['pid'] = array(
    '#type' => 'value',
    '#value' => $edit['pid'],
  );
  $form['type'] = array(
    '#type' => 'value',
    '#value' => $edit['data'],
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Template name'),
    '#default_value' => $edit['name'],
    '#maxlength' => 215,
    '#required' => TRUE,
  );

  $ref_node = '';
  if ($edit['nid']) {
    $ref_node = node_load(array('nid' => $edit['nid']));
  }
  $ref_user = '';
  if ($edit['uid']) {
    $ref_user = user_load(array('uid' => $edit['uid']));
  }
  $form['crc'] = array(
    '#tree' => TRUE,
  );
  foreach (crc_module_implements('crc_preset_form') as $module) {
    if (empty($settings[$module])) {
      $settings[$module] = array();
    }
    $func = $module .'_crc_preset_form';
    $form['crc'][$module] = $func($settings[$module], $form, $form_state);
  }
  if (module_exists('token')) {
    $form['token_help'] = array(
      '#type' => 'fieldset',
      '#title' => t('Token help'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['token_help']['text'] = array(
      '#type' => 'markup',
      '#value' => theme_crc_token_help(),
    );
  }
  $form['nid'] = array(
    '#type' => 'textfield',
    '#title' => t('Reference content item'),
    '#default_value' => $ref_node ? $ref_node->nid .':'. $ref_node->title : '',
    '#autocomplete_path' => 'crc/node/autocomplete',
    '#description' => t('Use the content item nid with optional title like this: "nid:title". Access checks for viewing this content item could be bypassed.'),
  );
  $form['uid'] = array(
    '#type' => 'textfield',
    '#title' => t('Reference user'),
    '#default_value' => $ref_user ? $ref_user->name : '',
    '#autocomplete_path' => 'user/autocomplete',
    '#description' => t('Access checks for viewing this user could be bypassed.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  if (!empty($edit['pid'])) {
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'));
  }
  return $form;
}

function theme_crc_token_help() {
  $output = '<p>'. t('The following tokens are available for use in flagged fields:') .'</p>';
  $tokens = array(
    '[node:xxxxx]' => t('Base related content item.'),
    '[user:xxxxx]' => t('The logged in user.'),
    '[ref-node:xxxxx]' => t('Optional existing content item to use as a template. Returns an empty string if the reference content item is not set.'),
    '[ref-user:xxxxx]' => t('Optional existing user to use as a template. Returns an empty string if the reference user is not set.'),
    '[alt-node:xxxxx]' => t('<strong>Depreciated.</strong> Will be removed from future releases.'),
    '[alt-user:xxxxx]' => t('<strong>Depreciated.</strong> Will be removed from future releases..'),
  );
  $output .= '<dl>';
  foreach ($tokens as $token => $details) {
    $output .= '<dt>'. $token .'</dt><dd>'. $details .'</dd>';
  }
  $output .= '</dl>';
  $output .= '<p>'. t('To use the tokens below, substitute the tokens given below with the corresponding prefix.') .'<br/>'. t('For example, to use the user token [user], use [user:user] for the logged in user or [ref-user:user] for the specified reference user.') .'</p>';
  $output .= theme('token_help', 'crc');
  return $output;
}

function crc_presets_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $nid = 0;
  if (!empty($values['nid'])) {
    list($nid, ) = explode(':', $values['nid']);
  }
  $uid = 0;
  if (!empty($values['uid'])) {
    if ($account = user_load(array('name' => $values['uid']))) {
      $uid = $account->uid;
    }
  }
  $values = array(
    'name' => $form_state['values']['name'],
    'nid' => $nid,
    'uid' => $uid,
    'data' => serialize($form_state['values']['crc']),
  );
  if (empty($form_state['values']['pid'])) {
    drupal_write_record('crc_preset', $values);
    $message = 'Template %name has been created.';
  }
  else {
    $values['pid'] = $form_state['values']['pid'];
    drupal_write_record('crc_preset', $values, 'pid');
    $message = 'Template %name has been updated.';
  }
  drupal_set_message(t($message, array('%name' => $values['name'])));

  $form_state['redirect'] = 'admin/settings/crc';
}

function crc_presets_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  // Ensure that the name is unique
  if (empty($values['pid'])) {
    $count = db_result(db_query("SELECT count(*) FROM {crc_preset} WHERE name = '%s'", $values['name']));
  }
  else {
    $count = db_result(db_query("SELECT count(*) FROM {crc_preset} WHERE name = '%s' AND pid <> %d", $values['name'], $values['pid']));
  }
  if ($count) {
    form_set_error('name', t('The name you have chosen is already in use.'));
  }

  if (!empty($values['nid'])) {
    list($nid, ) = explode(':', $values['nid']);
    if (!node_load(array('nid' => $nid))) {
      form_set_error('nid', t('The node was not found (nid: %d).', array('%d' => $nid)));
    }
  }

  if (!empty($values['uid'])) {
    if (!user_load(array('name' => $values['uid']))) {
      form_set_error('uid', t('The user %user was not found.', array('%user' => $values['uid'])));
    }
  }

  foreach (crc_module_implements('crc_preset_form_validate') as $module) {
    $func = $module .'_crc_preset_form_validate';
    $func($form, $form_state, $form_state['values']['crc'][$module]);
  }
}

/**
 * Page to edit a vocabulary.
 */
function crc_preset_edit($pid) {
  if ((isset($_POST['op']) && $_POST['op'] == t('Delete')) || isset($_POST['confirm'])) {
    return drupal_get_form('crc_preset_delete_form', $pid);
  }

  return drupal_get_form('crc_presets_form', crc_load($pid));
}

function crc_preset_delete_form($form_state, $pid) {
  $preset = crc_load($pid);
  if (!$preset) {
    drupal_set_message(t('The template could not be found.'), 'error');
    drupal_goto('admin/settings/crc');
  }
  $form = array();
  $form['pid'] = array('#type' => 'value', '#value' => $preset['pid']);
  $form['#preset'] = $preset;
  return confirm_form(
    $form,
    t('Are you sure you want to delete the template %preset?',
      array('%preset' => $preset['name'])
    ),
    'admin/settings/crc',
    t('This action cannot be undone.'),
    t('Delete'),  t('Cancel')
  );
}

function crc_preset_delete_form_submit($form, &$form_state) {
  $preset_usage = crc_preset_usage();
  $pid = (int) $form_state['values']['pid'];
  $system_pid = variable_get('crc_settings', 0);
  // Deleting system preset, set the default to 0
  if ($pid == $system_pid) {
    variable_set('crc_settings', 0);
    $system_pid = 0;
  }
  if (isset($preset_usage[$pid])) {
    foreach ($preset_usage[$pid] as $type => $name) {
      variable_set('crc_settings_'. $type, $system_pid);
    }
  }
  db_query("DELETE FROM {crc_preset} WHERE pid = %d", $pid);
  $ret = array();
  drupal_set_message(t('Template %preset was deleted.', array('%preset' => $form['#preset']['name'])));
  $form_state['redirect'] = 'admin/settings/crc';
}
