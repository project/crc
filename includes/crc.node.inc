<?php

/**
 * @file
 * This handles the crc pseudo-hooks for the following modules:
 *  node & filter
 *
 * It implement the following pseudo-hooks
 *
 *  hook_crc_node_form($org_form, $form_state)
 *  hook_crc_node_prepopulate(&$node, $settings, $values, $objects)
 *  hook_crc_preset_form($settings, $org_form, $form_state)
 *  hook_crc_preset_form_submit(&$values, $form, &$form_state)
 *  hook_crc_settings(&$settings)
 *  // TODO
 *  hook_crc_node_form_validate($form_state, $base_node)
 */

/**
 * @var int(3) Defines "Use lorem ipsum".
 */
define('CRC_LOREM_IPSUM', 3);

/**
 * @var int(2) Preset value @see CRC_PRESET int(3).
 */
define('CRC_PRESET_TIME', 2);

/**
 * @var int(5) Preset value @see CRC_PRESET int(3).
 */
define('CRC_PRESET_BODY', 5);

/**
 * Implementation of hook_crc_node_form().
 *
 * This provides the additional form elements to show on the
 * create related content page.
 *
 * @param $form_state
 * @param $base_node Parent node
 * @return $form
 */
function node_crc_node_form($form_state, $base_node) {
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
  );
  return $form;
}

/**
 * Helper function to set the default values.
 *
 * @param $settings
 * @return void
 */
function node_crc_settings(&$settings = array()) {
  $settings += array(
    'body' => array(),
    'author' => array(),
  );
  $settings['body'] += array(
    'options' => CRC_LOREM_IPSUM,
    'lorem_ipsum' => array(),
    'format' => CRC_DEFAULT,
    'format_custom' => variable_get('filter_default_format', 1),
  );
  $settings['body']['lorem_ipsum'] += array(
    'prefix' => 1,
    'template' => '<p>[lorem_ipsum]</p>',
    'paragraph_count' => 2,
  );
  $settings['author'] += array(
    'user' => CRC_DEFAULT,
    'user_custom' => '',
    'time' => CRC_PRESET_TIME,
    'time_custom' => '',
  );
}

/**
 * Implementation of hook_crc_node_prepopulate().
 *
 * This prepopulates the node object before it is passed
 * to the node_form.
 *
 * @param $node
 * @param $settings
 * @param $values
 * @param $objects
 * @return void
 */
function node_crc_node_prepopulate(&$node, $settings, $values, $objects) {
  global $user;
  $node->nid = NULL;
  $node->vid = NULL;
  $node->title = $values['name'];
  $parse_body = FALSE;
  switch ($settings['body']['options']) {
    case CRC_CLONE_NODE:
      $node->body = isset($objects['node']) ? $objects['node']->body : '';
      break;
    case CRC_CLONE_ALT_NODE:
      $node->body = isset($objects['ref-node']) ? $objects['ref-node']->body : '';
      $parse_body = TRUE;
      break;
    case CRC_LOREM_IPSUM:
      $node->body = crc_lorem_ipsum($settings['body']['lorem_ipsum']['paragraph_count'], $settings['body']['lorem_ipsum']['template'], $settings['body']['lorem_ipsum']['prefix']);
      break;
    case CRC_PRESET_BODY:
      $parse_body = TRUE;
      $node->body = $settings['body']['custom']['text'];
      break;
    case CRC_DEFAULT:
    default:
      $node->body = '';
      break;
  }

  if ($parse_body && module_exists('token')) {
    $node->body = crc_token_replace($node->body, $objects);
  }

  $node->teaser = node_teaser($node->body);

  $node->format = variable_get('filter_default_format', 1);
  switch ($settings['body']['format']) {
    case CRC_CLONE_NODE:
      $node->format = isset($objects['node']) ? $objects['node']->format : $node->format;
      break;
    case CRC_CLONE_ALT_NODE:
      $node->format = isset($objects['ref-node']) ? $objects['ref-node']->format : $node->format;
      break;
    case CRC_PRESET:
      $node->format = $settings['body']['format_custom'];
      break;
  }
  $node->filter = $node->format;

  $node->name = $user->name;
  $node->uid = $user->uid;

  switch ($settings['author']['user']) {
    case CRC_CLONE_NODE:
      if (isset($objects['node'])) {
        $node->name = $objects['node']->name;
        $node->uid = $objects['node']->uid;
      }
      break;
    case CRC_CLONE_ALT_NODE:
      if (isset($objects['ref-node'])) {
        $node->name = $objects['ref-node']->name;
        $node->uid = $objects['ref-node']->uid;
      }
      break;
    case CRC_CLONE_ALT_USER:
      if (isset($objects['ref-user'])) {
        $node->name = $objects['ref-user']->name;
        $node->uid = $objects['ref-user']->uid;
      }
      break;
    case CRC_PRESET:
      $node->name = '';
      $node->uid = NULL;
      if ($account = user_load(array('name' => $settings['author']['user_custom']))) {
        $node->name = $account->name;
        $node->uid = $account->uid;
      }
      break;
  }
  switch ($settings['author']['time']) {
    case CRC_CLONE_NODE:
      $node->date = isset($objects['node']) ? format_date($objects['node']->created, 'custom', 'Y-m-d H:i:s O') : '';
      break;
    case CRC_CLONE_ALT_NODE:
      $node->date = isset($objects['ref-node']) ? format_date($objects['ref-node']->created, 'custom', 'Y-m-d H:i:s O') : '';
      break;
    case CRC_PRESET_TIME:
    default:
      if (empty($settings['author']['time_custom'])) {
        $node->date = '';
      }
      else {
        $node->date = $settings['author']['time_custom'];
      }
      break;
  }
}

/**
 * Implementation of hook_crc_preset_form().
 *
 * This provides the form elements to show on the edit content
 * type form. These are used to set the defaults for the
 * content type.
 *
 * @param $settings
 * @param $org_form
 * @param $form_state
 * @return $form
 */
function node_crc_preset_form($settings, $org_form, $form_state) {
  $form = array('body' => array(), 'author' => array());

  $body_options = array(
    CRC_CLONE_NODE => t('Clone'),
    CRC_DEFAULT => t('Empty'),
    CRC_CLONE_ALT_NODE => t('Copy reference content item body'),
    CRC_LOREM_IPSUM => t('Lorem ipsum'),
    CRC_PRESET_BODY => t('Custom'));

  $form['body']['options'] = array(
    '#title' => t('Body settings'),
    '#type' => 'radios',
    '#options' =>  $body_options,
    '#default_value' => $settings['body']['options'],
    '#attributes' => array('class' => 'crc-node-content'),
    '#description' => t('The %preset and %ref options have integrated token support.', array('%preset' => t('Custom'), '%ref' => t('Copy reference content item body'))),
  );

  $form['body']['custom']['text'] = array(
    '#type' => 'textarea',
    '#title' => t('Custom body'),
    '#default_value' => $settings['body']['custom']['text'],
    '#cols' => 40,
    '#rows' => 8,
    '#attributes' => array('class' => 'crc-node-content-'. CRC_PRESET_BODY),
  );


  $form['body']['lorem_ipsum']['prefix'] = array(
    '#type' => 'checkbox',
    '#title' => t('Prefix the first paragraph with the text "Lorem ipsum...".'),
    '#default_value' => $settings['body']['lorem_ipsum']['prefix'],
    '#description' => t('This option appends the text %ll to the first paragraph of lorem ipsum.', array('%ll' => t('Lorem ipsum dolor sit amet, consectetur adipiscing elit.'))),
    '#attributes' => array('class' => 'crc-node-content-'. CRC_LOREM_IPSUM),
  );

  $form['body']['lorem_ipsum']['template'] = array(
    '#type' => 'textfield',
    '#title' => t('Default lorem ipsum paragraph template'),
    '#default_value' => $settings['body']['lorem_ipsum']['template'],
    '#description' => t('Insert the tag !tag where you want the lorem ipsum text to go. If the tag is missing, only the template text is inserted. Note that a single newline to appended to every paragraph inserted.', array('!tag' => '[lorem_ipsum]')),
    '#attributes' => array('class' => 'crc-node-content-'. CRC_LOREM_IPSUM),
  );

  $form['body']['lorem_ipsum']['paragraph_count'] = array(
    '#type' => 'select',
    '#title' => t('Number of paragraphs to add'),
    '#options' => drupal_map_assoc(range(1, 10)),
    '#default_value' => $settings['body']['lorem_ipsum']['paragraph_count'],
    '#attributes' => array('class' => 'crc-node-content-'. CRC_LOREM_IPSUM),
  );

  $form['body']['format'] = array(
    '#title' => t('Body filter settings'),
    '#type' => 'radios',
    '#options' =>  array(
      CRC_CLONE_NODE => t('Clone'),
      CRC_DEFAULT => t('Default'),
      CRC_CLONE_ALT_NODE => t('Copy reference content item body filter'),
      CRC_PRESET => t('Custom')),
    '#default_value' => $settings['author']['user'],
    '#attributes' => array('class' => 'crc-node-filter'),
    '#description' => t('Currently, you can not bypass user restrictions on the format. Select an option that the user is likely to have.'),
  );
  $formats = array();
  foreach (filter_formats() as $format) {
    $formats[$format->format] = $format->name;
  }
  $form['body']['format_custom'] = array(
    '#title' => t('Body custom settings'),
    '#type' => 'radios',
    '#prefix' => '<div id="edit-crc-node-body-format-custom-2-parent-wrapper">',
    '#suffix' => '</div>',
    '#options' =>  $formats,
    '#attributes' => array('class' => 'crc-node-filter-'. CRC_PRESET),
    '#default_value' => $settings['body']['format_custom'] ? $settings['body']['format_custom'] : variable_get('filter_default_format', 1),
  );

  $form['author'] = array(
    '#tree' => TRUE,
  );
  $form['author']['user'] = array(
    '#title' => t('Author settings'),
    '#type' => 'radios',
    '#options' =>  array(
      CRC_CLONE_NODE => t('Clone'),
      CRC_DEFAULT => t('Logged in user'),
      CRC_CLONE_ALT_NODE => t('Copy reference content item author'),
      CRC_CLONE_ALT_USER => t('Copy reference user'),
      CRC_PRESET => t('Custom')),
    '#default_value' => $settings['author']['user'],
    '#attributes' => array('class' => 'crc-node-author'),
  );

  $user_default = $settings['author']['user_custom'];

  $form['author']['user_custom'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom authored by'),
    '#maxlength' => 60,
    '#autocomplete_path' => 'user/autocomplete',
    '#default_value' => $user_default,
    '#description' => t('Leave blank for %anonymous.', array('%anonymous' => variable_get('anonymous', t('Anonymous')))),
    '#attributes' => array('class' => 'crc-node-author-'. CRC_PRESET),
  );

  $form['author']['time'] = array(
    '#title' => t('Timestamp settings'),
    '#type' => 'radios',
    '#options' =>  array(
      CRC_CLONE_NODE => t('Clone'),
      CRC_CLONE_ALT_NODE => t('Copy reference content item creation time'),
      CRC_PRESET_TIME => t('Custom')),
    '#default_value' => $settings['author']['time'],
    '#attributes' => array('class' => 'crc-node-created'),
  );
  $time_default = $settings['author']['time_custom'] ? $settings['author']['time_custom'] : '';
  $form['author']['time_custom'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom authored on'),
    '#maxlength' => 25,
    '#default_value' => $time_default,
    '#description' => t('Format: %time. Leave blank to use the time of form submission.', array('%time' => format_date(time(), 'custom', 'Y-m-d H:i:s O'))),
    '#attributes' => array('class' => 'crc-node-created-'. CRC_PRESET_TIME),
  );
  return $form;
}

function node_crc_preset_form_validate($form, &$form_state, $values) {
  if (!empty($values['author']['user_custom'])) {
    if (!user_load(array('name' => $values['author']['user_custom']))) {
      form_set_error('crc][node][author][user_custom', t('The user %user was not found.', array('%user' => $values['author']['user_custom'])));
    }
  }
}

/**
 * Implementation of hook_crc_preset_form_submit().
 *
 * This updates any of the form values before they are saved.
 *
 * @param $values
 * @param $form
 * @param $form_state
 * @return void
 */
function node_crc_preset_form_submit(&$values, $form, &$form_state) {
  if ($account = user_load(array('name' => $values['author']['user_custom']))) {
    $values['author']['user_custom'] = $account->uid;
  }
  else {
    $values['author']['user_custom'] = '';
  }
  $values['author']['time_custom'] = !empty($values['author']['time_custom']) ? strtotime($values['author']['time_custom']) : '';
}
