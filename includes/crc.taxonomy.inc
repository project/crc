<?php

/**
 * @file
 * This handles the crc pseudo-hooks for the following modules:
 *  taxonomy
 *
 * It implement the following pseudo-hooks
 *
 *  hook_crc_node_prepopulate(&$node, $settings, $values, $objects)
 *  hook_crc_preset_form($settings, $org_form, $form_state)
 *  hook_crc_settings($settings)
 */

/**
 * Helper function
 *
 * Sets the default values to use.
 *
 * @param $settings
 * @return void
 */
function taxonomy_crc_settings(&$settings = array()) {
  $settings += array(
    'taxonomy' => CRC_CLONE_NODE,
  );
}

/**
 * Implementation of hook_crc_node_prepopulate().
 *
 * This prepopulates the node object before it is passed
 * to the node_form.
 *
 * @param $node
 * @param $settings
 * @param $values
 * @param $objects
 * @return void
 */
function taxonomy_crc_node_prepopulate(&$node, $settings, $values, $objects) {
  // $node, $settings[$module], $values, $objects
  $taxonomy = array();

  if ($settings['taxonomy'] == CRC_CLONE_NODE
      && !empty($objects['node']) && isset($objects['node']->taxonomy)) {
    $taxonomy = $objects['node']->taxonomy;
  }
  if ($settings['taxonomy'] == CRC_CLONE_ALT_NODE
      && !empty($objects['ref-node']) && isset($objects['ref-node']->taxonomy)) {
    $taxonomy = $objects['ref-node']->taxonomy;
  }

  $node->taxonomy = $taxonomy;
}

/**
 * Implementation of hook_crc_preset_form().
 *
 * This provides the form elements to show on the edit content
 * type form. These are used to set the defaults for the
 * content type.
 *
 * @param $settings
 * @param $org_form
 * @param $form_state
 * @return $form
 */
function taxonomy_crc_preset_form($settings, $org_form, $form_state) {
  $form = array();
  $taxonomy_options = array(
    CRC_CLONE_NODE => t('Clone'),
    CRC_CLONE_ALT_NODE => t('Copy reference content item'),
    CRC_EMPTY => t('Empty'));
  $form['taxonomy'] = array(
    '#title' => t('Taxonomy settings'),
    '#type' => 'radios',
    '#options' =>  $taxonomy_options,
    '#default_value' => $settings['taxonomy'],
  );

  return $form;
}
