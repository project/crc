<?php

/**
 * @file
 * This handles the crc pseudo-hooks for the following modules:
 *  menu
 *
 *  It implement the following pseudo-hooks
 *
 *  hook_crc_settings(&$settings = array())
 *  hook_crc_node_form($org_form, $form_state)
 *  hook_crc_node_prepopulate(&$node, $settings, $values, $objects)
 *  hook_crc_preset_form($settings, $org_form, $form_state)
 */

define('CRC_MENU_SIBLING', 1);
define('CRC_MENU_CHILD', 2);
define('CRC_MENU_ALT_NODE_SIBLING', 4);
define('CRC_MENU_ALT_NODE_CHILD', 5);

define('CRC_MENU_COPY_PREPOPULATE_TITLE', 2);

/**
 * Implementation of hook_crc_node_form().
 *
 * This provides the additional form elements to show on the
 * create related content page.
 *
 * @param $form_state
 * @param $base_node Parent node
 * @return $form
 */
function menu_crc_node_form($form_state, $base_node) {
  $options = array(
    CRC_EMPTY => t('Unrelated'),
    CRC_MENU_SIBLING => t('Sibling'),
    CRC_MENU_CHILD => t('Child'),
    CRC_PRESET => t('Preconfigured'));
  $form = array();
  if (user_access('administer menu')) {
    $form['menu_parent'] = array(
      '#type' => 'radios',
      '#title' => t('Menu relationship'),
      '#options' => $options,
      '#default_value' => key($options),
    );
  }
  return $form;
}

/**
 * Helper function
 *
 * Sets the default values to use.
 *
 * @param $settings
 * @return void
 */
function menu_crc_settings(&$settings = array()) {
  $settings += array(
    'menu_name' => CRC_EMPTY,
    'menu_name_custom' => '',
    'menu_parent' => CRC_EMPTY,
    'menu_parent_custom' => 'primary-links:0',
  );
}

/**
 * Implementation of hook_crc_node_prepopulate().
 *
 * This prepopulates the node object before it is passed
 * to the node_form.
 *
 * @param $node
 * @param $settings
 * @param $values
 * @param $objects
 * @return void
 */
function menu_crc_node_prepopulate(&$node, $settings, $values, $objects) {
  // Sets the default menu item (blank)
  $node->menu = crc_default_menu();

  // Flag for setting title
  $has_relationship = FALSE;

  $switch_key = $settings['menu_parent'];
  // Check user access before accepting options
  if (user_access('administer menu') && array_key_exists('menu_parent', $values)) {
    $switch_key = $values['menu_parent'];
  }

  switch ($switch_key) {
    case CRC_MENU_SIBLING:
    case CRC_MENU_CHILD:
      $node->menu['menu_name'] = $objects['node']->menu['menu_name'];
      $node->menu['plid'] = ($switch_key == CRC_MENU_SIBLING) ? $objects['node']->menu['plid'] : $objects['node']->menu['mlid'];
      $has_relationship = TRUE;
      break;
    case CRC_MENU_ALT_NODE_SIBLING:
    case CRC_MENU_ALT_NODE_CHILD:
      $node->menu['menu_name'] = $objects['ref-node']->menu['menu_name'];
      $node->menu['plid'] = ($switch_key == CRC_MENU_ALT_NODE_SIBLING) ? $objects['ref-node']->menu['plid'] : $objects['ref-node']->menu['mlid'];
      $has_relationship = TRUE;
      break;
    case CRC_PRESET: // Custom
      if (!empty($settings['menu_parent_custom'])) {
        list($menu_name, $mlid) = explode(':', $settings['menu_parent_custom']);
        if (!empty($menu_name) && strlen((string)$mlid)) {
          $node->menu['menu_name'] = $menu_name;
          $node->menu['plid'] = $mlid;
        }
      }
      break;
    case CRC_EMPTY:
    default:
      break;
  }

  switch ($settings['menu_name']) {
    case CRC_MENU_COPY_PREPOPULATE_TITLE:
      // No token support for user submitted data
      $node->menu['link_title'] = $values['name'];
      break;
    case CRC_PRESET:
      $node->menu['link_title'] = $settings['menu_name_custom'];
      if (module_exists('token')) {
        $node->menu['link_title'] = crc_token_replace($node->menu['link_title'], $objects);
      }
      break;
    case CRC_EMPTY:
    default:
      $node->menu['link_title'] = '';
      break;
  }
  // A relationship has been given, but defaults are empty
  // so copy user submitted values for admins.
  if (user_access('administer menu') && $has_relationship && empty($node->menu['link_title'])) {
    // No token support for user submitted data
    $node->menu['link_title'] = $values['name'];
  }
}

/**
 * Implementation of hook_crc_preset_form().
 *
 * This provides the form elements to show on the edit content
 * type form. These are used to set the defaults for the
 * content type.
 *
 * @param $settings
 * @param $org_form
 * @param $form_state
 * @return $form
 */
function menu_crc_preset_form($settings, $org_form, $form_state) {
  $form = array();

  if (user_access('administer menu')) {
    $menu_options = array(
      CRC_EMPTY => t('None'),
      CRC_MENU_COPY_PREPOPULATE_TITLE => t('Copy user specified title'),
      CRC_PRESET => t('Custom'));
    $form['menu_name'] = array(
      '#title' => t('Default menu link title'),
      '#type' => 'radios',
      '#options' =>  $menu_options,
      '#default_value' => $settings['menu_name'],
      '#attributes' => array('class' => 'crc-menu-options'),
      '#description' => t('The %title field has integrated token support.', array('%title' => t('Custom'))),
    );

    $form['menu_name_custom'] = array(
      '#type' => 'textfield',
      '#title' => t('Custom menu link title'),
      '#default_value' => $settings['menu_name_custom'],
      '#attributes' => array('class' => 'crc-menu-options-'. CRC_PRESET),
    );

    $menu_parent = array(
      CRC_EMPTY => t('Unrelated'),
      CRC_MENU_SIBLING => t('Sibling'),
      CRC_MENU_CHILD => t('Child'),
      CRC_MENU_ALT_NODE_SIBLING => t('Sibling of reference content item'),
      CRC_MENU_ALT_NODE_CHILD => t('Child of reference content item'),
      CRC_PRESET => t('Custom parent menu item'));

    $form['menu_parent'] = array(
      '#title' => t('Default parent item behaviour'),
      '#type' => 'radios',
      '#options' =>  $menu_parent,
      '#default_value' => $settings['menu_parent'],
      '#description' => t('This selects the default relationship of the new content in relation to the existing content item when; the user does not have administrate menu permission or if the template is called directly.'),
      '#attributes' => array('class' => 'crc-menu-parent'),
    );

    // Generate a list of possible parents
    $form['menu_parent_custom'] = array(
      '#type' => 'select',
      '#title' => t('Custom parent item'),
      '#default_value' => $settings['menu_parent_custom'],
      '#options' => menu_parent_options(menu_get_menus(), crc_default_menu()),
      '#description' => t('The maximum depth for an item and all its children is fixed at !maxdepth. Some menu items may not be available as parents if selecting them would exceed this limit.', array('!maxdepth' => MENU_MAX_DEPTH)),
      '#attributes' => array('class' => 'crc-menu-parent-'. CRC_PRESET),
    );
  }
  else {
    // the values are saved directly, so add these here
    foreach (array('menu_name', 'menu_name_custom', 'menu_parent', 'menu_parent_custom') as $key) {
      $form[$key] = array(
        '#type' => 'value',
        '#value' => $settings[$key],
      );
    }
  }
  return $form;
}
