
Drupal.behaviors.crc = function(context) {
  Drupal.crc.init(context);
};

Drupal.crc = {
  init : function(context) {
    $('input.crc-menu-options').change(Drupal.crc.toogleOptions);
    $('input.crc-menu-options:checked').each(Drupal.crc.toogleOptions);
    
    $('input.crc-menu-parent').change(Drupal.crc.toogleOptions);
    $('input.crc-menu-parent:checked').each(Drupal.crc.toogleOptions);
    
    $('input.crc-node-content').change(Drupal.crc.toogleOptions);
    $('input.crc-node-content:checked').each(Drupal.crc.toogleOptions);
    
    $('input.crc-node-content').change(Drupal.crc.toogleOptions);
    $('input.crc-node-content:checked').each(Drupal.crc.toogleOptions);
    
    $('input.crc-node-filter').change(Drupal.crc.toogleOptions);
    $('input.crc-node-filter:checked').each(Drupal.crc.toogleOptions);
    
    $('input.crc-node-author').change(Drupal.crc.toogleOptions);
    $('input.crc-node-author:checked').each(Drupal.crc.toogleOptions);
    
    $('input.crc-node-created').change(Drupal.crc.toogleOptions);
    $('input.crc-node-created:checked').each(Drupal.crc.toogleOptions);
    
  },
  toogleOptions : function() {
    var crcClass = $(this).getCustomClassName();
    $('.' + crcClass + '-' + $(this).val()).each(function () {
      $('#' + this.id + '-wrapper').show();
      $('#' + this.id + '-parent-wrapper').show();
    });
    $('input.' + crcClass).not(this).each(function() {
      $('.' + crcClass + '-' + $(this).val()).each(function () {
        $('#' + this.id + '-wrapper').hide();
        $('#' + this.id + '-parent-wrapper').hide();
      });
    });
  },
  insertImage : function(event) {

  }
}

$.fn.getCustomClassName = function() {
  if (name = this.attr("className")) {
    var classes = name.split(" ");
    classes = $.grep(classes, function (value) {
      var prefix = 'crc-';
      if(value.length < prefix.length) return false;
      return (value.substring(0, prefix.length) == prefix);
    });
    return classes.length ? classes[0] : false;
  }
  else {
    return false;
  }
};

