<?php

/**
 * @file
 * Node creation functionality.
 */

function crc_add_page_form(&$form_state, $base_node) {
  $type_options = crc_add_node_type_options();
  // The same form is being used by the block and form page
  $arg1 = arg(1);
  if (arg(0) == 'node' && is_numeric($arg1) && arg(2) == 'add_related') {
    drupal_set_title(t('Create related content for %title', array('%title' => $base_node->title)));
  }

  $form = array();
  $form['type'] = array(
    '#type' => 'radios',
    '#title' => t('Type'),
    '#options' => $type_options,
    '#default_value' => isset($type_options[$base_node->type]) ? $base_node->type : key($type_options),
    '#required' => TRUE,
    '#weight' => -50,
  );
  if (count($type_options) >= 5) {
    $form['type']['#type'] = 'select';
  }
  // Copy from content.module
  $default_weights = array(
    'node' => -5,
    'locale' => 0,
    'menu' => -2,
    'taxonomy' => -3,
    'book' => 10,
    'poll' => -3,
    'upload' => 30,
  );
  foreach (crc_module_implements('crc_node_form') as $module) {
    $func = $module .'_crc_node_form';
    $form[$module] = $func($form_state, $base_node);
    if (isset($default_weights[$module]) && !isset($form[$module]['#weight'])) {
      $form[$module]['#weight'] = $default_weights[$module];
    }
  }
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $base_node->nid,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Next'),
  );
  return $form;
}

function crc_clean_query($data, $exclude = array('op', 'submit', 'form_build_id', 'form_token', 'form_id', 'type', 'nid')) {
  $query = array();
  foreach ($data as $key => $value) {
    if (!in_array($key, $exclude)) {
      $query[$key] = $value;
    }
  }
  return $query;
}

function crc_add_page_form_submit($form, &$form_state) {
  drupal_goto('node/add/'. str_replace('_', '-', $form_state['values']['type']) .'/crc/'. $form_state['values']['nid'], crc_clean_query($form_state['values']));
}

function crc_node_add($type, $base_node, $pid = NULL) {
  $output = '';
  $types = node_get_types();
  $type = isset($type) ? str_replace('-', '_', $type) : NULL;

  // If a node type has been specified, validate its existence.
  if (isset($types[$type]) && node_access('create', $type)) {
    // Initialize settings:
    $node = new stdClass();
    $node->type = $type;
    module_load_include('inc', 'node', 'node.pages');
    foreach (array('body', 'title', 'format') as $key) {
      $node->$key = NULL;
    }
    node_object_prepare($node);

    // No settings bypass the prepopulate step
    if ($settings = ($pid ? crc_system_settings($pid) : crc_settings($type))) {
      crc_prepare_node($node, $settings, $base_node);
    }
    drupal_set_title(t('Create @name', array('@name' => $types[$type]->name)));
    $output = drupal_get_form($type .'_node_form', $node);
  }

  return $output;
}

function crc_prepare_node(&$node, $settings, $base_node = NULL) {
  global $user;

  // Prepare the potential token / default objects
  if (isset($base_node)) {
    node_object_prepare($base_node);
  }

  // Prepare the logged in user
  $account = drupal_clone($user);
  if (module_exists('profile')) {
    profile_load_profile($account);
  }

  // Prepare the alternative objects
  $ref_node = FALSE;
  if (!empty($settings['nid'])) {
    if ($ref_node = node_load(array('nid' => $settings['nid']))) {
      node_object_prepare($ref_node);
    }
  }
  $ref_user = FALSE;
  if (!empty($settings['uid'])) {
    $ref_user = node_load(array('uid' => $settings['uid']));
    if ($ref_user->uid && module_exists('profile')) {
      profile_load_profile($ref_user);
    }
  }

  $objects = array(
    'node' => $base_node,
    'user' => $account,
    'ref-node' => $ref_node,
    'ref-user' => $ref_user,
  );
  $values = $_GET;
  unset($values['q']);
  foreach (crc_module_implements('crc_node_prepopulate') as $module) {
    $func = $module .'_crc_node_prepopulate';
    $func($node, $settings[$module], $values, $objects);
  }
}

function crc_lorem_ipsum($paragraphs = 3, $template = '[lorem_ipsum]', $prefix = TRUE) {
  $prefix = $prefix ? t('Lorem ipsum dolor sit amet, consectetur adipiscing elit.') .' ' : '';
  $paragraphs = (int) $paragraphs;
  if (empty($template) || strpos($template, '[lorem_ipsum]') === FALSE) {
    $template = '[lorem_ipsum]';
  }
  $template .= "\n";
  $lorem_ipsum = array(
      'Morbi consequat egestas sem. Sed ac pede. Cras convallis pede eget ligula. Suspendisse vitae augue ut tellus vehicula volutpat. Fusce iaculis mattis eros. Pellentesque nec lorem vitae ligula pharetra aliquet. Vestibulum eu nisi. Mauris nibh enim, cursus malesuada, ultricies vel, facilisis non, arcu. Vivamus pellentesque cursus libero. Mauris lacus elit, bibendum vel, pretium non, suscipit sed, sapien. Fusce libero sem, commodo ut, rhoncus ac, lobortis nec, mi. Nullam eu metus. Donec sit amet nibh. Maecenas pulvinar. Suspendisse sodales lorem. Etiam ornare ipsum sed lacus. Suspendisse elementum augue et justo. Suspendisse neque erat, vulputate id, sodales et, vestibulum porta, elit. Ut posuere. Sed libero orci, tempor ac, iaculis in, vehicula sed, nibh.',
      'Sed laoreet condimentum odio. Pellentesque sapien purus, rutrum ut, pulvinar ac, fringilla in, magna. Cras imperdiet. Sed in nisi. Phasellus non enim a diam tempor adipiscing. Sed feugiat ligula sed ipsum. Ut ut quam. Integer eget enim non elit tempor molestie. In adipiscing quam a nibh. Vivamus tincidunt. Nulla libero libero, hendrerit vitae, aliquam vel, luctus et, elit. Phasellus mi felis, porta quis, interdum sit amet, viverra et, urna. Integer molestie vehicula purus. Donec nisi magna, gravida eget, vehicula nec, semper nec, mi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nunc elit felis, mollis ut, condimentum iaculis, imperdiet ac, ipsum. Vivamus ultricies elit at justo. Phasellus nec erat dignissim dolor pharetra consequat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.',
      'Nulla elit sapien, porttitor ornare, scelerisque quis, pellentesque et, tortor. Nullam adipiscing cursus magna. Integer fermentum ligula nec erat. Nunc malesuada blandit quam. Etiam urna purus, rutrum a, tincidunt sit amet, molestie quis, nibh. Vivamus et sapien et erat rhoncus mattis. Donec ut nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent quis risus. Praesent mollis ligula in orci. Donec commodo mauris et magna. Morbi volutpat urna in quam. In adipiscing, ipsum in scelerisque vulputate, tortor mi auctor purus, eu rhoncus ante sem adipiscing quam. Donec quam. Quisque rhoncus mi eu libero. Quisque eget quam. Maecenas non mauris ac lacus pellentesque vehicula. Phasellus interdum. Quisque blandit accumsan ipsum. Nulla nulla.',
      'Maecenas sodales ipsum. Phasellus sagittis bibendum ligula. Duis risus eros, lacinia a, fringilla et, hendrerit sit amet, risus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum sit amet neque. Praesent ut neque. Nunc pretium, magna ac ullamcorper dignissim, metus justo accumsan orci, sed laoreet leo diam in pede. Vivamus turpis pede, iaculis in, condimentum at, sollicitudin ut, tortor. Vestibulum ligula. Mauris ligula. Integer et urna. Suspendisse quam augue, imperdiet vitae, vestibulum nec, mollis ac, ante. Suspendisse eros odio, sagittis vel, dictum at, eleifend id, massa. Nulla dictum, mi id dignissim egestas, purus neque ultricies pede, sed convallis tellus ante vulputate libero. Aliquam erat volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse potenti. Phasellus iaculis pede quis elit.',
      'Suspendisse dolor. Nullam commodo. Nulla ac nisl vel diam posuere auctor. Curabitur in tortor. Aliquam erat volutpat. Quisque tristique urna porta eros. Donec non arcu et leo fermentum dignissim. Fusce cursus ornare justo. Morbi dui. Duis porttitor, metus ut ultrices vulputate, enim risus aliquam massa, et bibendum lorem turpis quis dui. Fusce commodo. Cras orci velit, dapibus id, sollicitudin ullamcorper, pharetra at, magna. Integer luctus purus at diam. Ut ac sapien.',
      'Sed interdum eros a lacus. Donec non nisl non risus pretium tincidunt. Morbi posuere gravida tellus. Nulla facilisi. Nulla blandit velit commodo orci. Integer in ante. Donec et nunc in enim consequat tempus. Fusce arcu neque, ullamcorper non, laoreet eu, fermentum vitae, nisi. Cras tortor mi, consectetur ut, pellentesque vel, vehicula quis, ante. Pellentesque malesuada. Proin non est. Curabitur feugiat.',
      'Ut porttitor enim vel purus. Aliquam turpis dolor, volutpat ut, mattis at, euismod quis, sapien. Proin quis magna. Sed ligula. Nunc ultricies, velit non aliquam ultrices, orci ante accumsan metus, non dapibus diam enim ac nisl. Mauris mollis, enim interdum dignissim euismod, neque lectus facilisis quam, luctus aliquet nisl nibh dapibus odio. Morbi sollicitudin. Morbi porta, quam eu ullamcorper fermentum, tellus erat ornare velit, quis lobortis ipsum ligula vel tortor. Maecenas commodo tellus a quam. Fusce risus. Maecenas eleifend condimentum libero. Nam bibendum massa eu nisi. Phasellus aliquam nulla ut eros.',
      'Sed faucibus. Cras pretium, erat a vestibulum consequat, sem ligula venenatis tellus, et convallis elit massa sed purus. Curabitur gravida nisl eu diam porta dictum. Nullam pede sem, tempor nec, fermentum in, hendrerit eu, purus. Nulla gravida felis vitae metus. Nunc scelerisque iaculis neque. Sed auctor metus accumsan eros. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut luctus luctus metus. Maecenas aliquam lectus at metus. Donec tincidunt sem et urna. Sed lobortis. Nunc bibendum, magna vel egestas feugiat, lorem est ornare velit, tristique dapibus erat nunc sed leo. Donec vitae est nec nunc porta adipiscing. Aenean non orci dignissim tortor accumsan euismod. Duis non eros. Fusce erat. Aenean mollis lobortis lacus. Cras viverra tortor nec elit ultricies ullamcorper. Nam facilisis.',
      'Vestibulum pretium facilisis libero. Etiam vehicula mauris condimentum massa. Quisque non elit eget risus aliquam tempor. In eu turpis. Vestibulum porta eros quis nibh. Praesent arcu massa, venenatis molestie, cursus vitae, tincidunt in, tortor. Integer viverra dictum metus. Duis sit amet leo. Fusce eleifend, purus eget egestas tempor, purus nisl fringilla pede, vitae laoreet mauris dui vel justo. Suspendisse pharetra ligula non pede. Nunc congue. In lorem. Sed a enim sit amet tellus elementum lacinia. In hac habitasse platea dictumst. Aenean posuere.',
      'Aliquam feugiat ipsum non turpis. Sed mollis gravida risus. Suspendisse lacinia velit at risus. Nam vel urna at est rutrum euismod. Aliquam erat volutpat. Vestibulum elit tortor, mattis nec, aliquam at, hendrerit quis, nisi. In vestibulum accumsan erat. Proin purus velit, gravida et, iaculis et, consequat et, ante. Sed consectetur. Donec neque ipsum, tempor eget, mollis suscipit, luctus ac, enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      'Quisque faucibus pretium libero. Vestibulum risus. Proin id nisl. In interdum, mauris ac ullamcorper aliquet, enim tortor tincidunt dolor, non adipiscing urna quam vitae mi. Vivamus vitae purus viverra nibh ornare tincidunt. Donec orci. Vivamus arcu arcu, fermentum in, lacinia nec, rhoncus quis, metus. Morbi dapibus est eu mi. Ut vel nisi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec ac dui pellentesque magna accumsan ornare. Nam augue. Nulla viverra, purus vitae vulputate iaculis, urna risus semper nulla, ac varius metus ligula ut sapien. Ut vitae quam.',
      'Nunc eu dui. Etiam dui sem, cursus et, semper at, porttitor sodales, est. Suspendisse laoreet turpis euismod magna. Pellentesque volutpat risus nec libero pellentesque bibendum. Aenean tincidunt luctus quam. Suspendisse sed lorem nec lectus tincidunt pellentesque. Mauris scelerisque turpis vel augue. Suspendisse pulvinar ante ut purus. Etiam congue. In nibh leo, lacinia ut, consequat ut, dictum nec, sapien. Aenean ut orci at lorem gravida tristique. Donec libero massa, mollis eget, pulvinar sagittis, mollis nec, erat. Sed imperdiet. Maecenas viverra, eros et eleifend tristique, tellus odio ornare lorem, a blandit erat dolor sit amet dolor. Nulla aliquam nibh vitae nibh. Sed in neque vitae lectus hendrerit bibendum. In scelerisque, turpis id consequat egestas, purus erat placerat metus, et blandit dui arcu ac nisi. Vivamus mattis turpis ac purus. Sed consequat, odio vel pellentesque interdum, libero mi rutrum dolor, sed suscipit augue lacus sed risus. Donec sed ligula sit amet mi facilisis tempus.',
      'In hac habitasse platea dictumst. Fusce sit amet libero. Curabitur nibh. Maecenas iaculis augue eu erat. Curabitur dignissim. Duis odio. Nunc imperdiet bibendum nulla. Donec accumsan urna pretium ligula. Fusce eget leo pulvinar nunc iaculis ultrices. Ut dictum, odio eget bibendum porta, leo sapien convallis felis, nec ultrices tellus turpis eget leo. In tristique orci at tellus. Cras hendrerit ligula ac purus. Sed ornare. Nulla ac lorem ut quam interdum sodales. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.',
      'Donec semper eros eu turpis. Phasellus diam. Nam tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla facilisi. Etiam risus neque, dictum vitae, malesuada non, adipiscing ut, dui. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam nulla risus, fringilla sed, placerat vel, mollis malesuada, est. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In hac habitasse platea dictumst. In non erat. Donec volutpat magna non nulla. Ut ut neque. Nunc eleifend, nisl vel volutpat vestibulum, justo libero scelerisque nulla, eget posuere eros purus eget libero. Mauris pretium, nisl quis pulvinar tempus, est nibh suscipit ipsum, ut tempus justo risus vehicula quam.',
      'Mauris lorem metus, facilisis sed, aliquet eu, euismod id, libero. Phasellus sed erat. Praesent a tellus tincidunt sapien tempus porttitor. Donec convallis arcu at quam rutrum bibendum. Curabitur pharetra. Sed eu magna. Integer consectetur. Donec nec enim id erat feugiat consequat. Sed mauris. Fusce tempor, eros nec tincidunt suscipit, leo tellus iaculis mi, ut aliquet mi nisi a lectus. In hac habitasse platea dictumst. Quisque venenatis, lacus ac iaculis ornare, erat erat pulvinar nibh, eu ullamcorper erat est et purus. Nullam ullamcorper aliquet turpis. Curabitur semper diam at urna. Aenean dolor odio, aliquam non, aliquet id, lobortis non, nunc. Vivamus imperdiet egestas ligula. Nullam eleifend vehicula leo. Ut molestie, nisi vel tristique aliquet, lectus leo euismod metus, eget ornare tortor velit id sem.',
      'Mauris suscipit tempus sapien. Curabitur euismod, mauris eu porta iaculis, ante augue vulputate nisi, quis viverra mi turpis a lacus. Mauris dapibus, dui a dictum sollicitudin, felis est rhoncus orci, vitae facilisis nisl sapien at dolor. Morbi dapibus laoreet erat. Aliquam eleifend pede non magna. Vivamus vitae nisl quis neque porta condimentum. Aenean pretium, nisi sed dapibus cursus, lectus ante molestie leo, condimentum facilisis enim lacus at felis. Quisque mollis convallis dolor. Integer vulputate lorem. Curabitur fringilla. Suspendisse mollis.',
      'Sed non est suscipit mi ultricies porta. Cras varius. Sed suscipit luctus lacus. Phasellus sed lorem. Maecenas mauris ligula, consectetur non, bibendum eget, varius vitae, eros. Aenean quis diam. Sed auctor sem sed velit porttitor mollis. Ut nisl tellus, consectetur vitae, fringilla euismod, ornare ac, metus. Duis euismod, mauris sit amet iaculis ultrices, augue ante hendrerit arcu, ac egestas mauris tortor id ligula. Nullam interdum dolor ac erat. In risus quam, egestas quis, facilisis eget, commodo eleifend, urna.',
      'Morbi eget quam. Aenean nisl. Vivamus pretium rhoncus pede. Etiam consectetur. Cras tortor tellus, porttitor eu, ultricies sit amet, egestas nec, arcu. Cras elementum orci. Duis et massa. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam egestas tincidunt ligula. Nunc commodo nisl at nisi. Aliquam ultricies venenatis risus. Quisque vehicula ornare metus. Integer vel nulla ac diam pharetra porta. Fusce lobortis neque a nibh. Nunc lacinia metus et diam.',
      'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec tempor metus tincidunt dolor. Proin non eros. Vestibulum ligula ipsum, blandit sed, commodo non, congue non, ipsum. Mauris ipsum arcu, consequat et, auctor id, ultricies at, eros. Nunc eu felis vitae nulla adipiscing lobortis. Phasellus porttitor nisi id diam. Quisque quam. Etiam dignissim rutrum lorem. Morbi a arcu vitae eros pharetra rutrum. Suspendisse sit amet diam. Sed tincidunt. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed iaculis tellus quis urna. Phasellus tincidunt, enim sit amet luctus faucibus, tortor leo vestibulum ante, eu varius mi ante nec orci. Donec bibendum sollicitudin leo.',
      'Aenean arcu nibh, tincidunt id, luctus eu, condimentum eget, est. Donec ipsum elit, scelerisque ut, semper sit amet, luctus ac, sem. Nunc placerat consequat sem. Maecenas nisl. Phasellus fringilla, nisl sed euismod tincidunt, ligula mi venenatis mauris, a viverra ante libero in neque. Nulla risus lacus, bibendum vel, dapibus et, convallis quis, eros. Donec scelerisque porttitor velit. Donec in tellus vitae odio rhoncus auctor. Sed euismod. Donec sed nisl. In rutrum.',
      'Morbi justo felis, commodo ut, sollicitudin ut, vulputate eget, risus. Etiam molestie, ligula vitae pharetra mollis, nulla pede laoreet nibh, vitae sodales ante nunc sit amet dolor. Aliquam erat volutpat. Integer sit amet risus. Duis in risus sed ante porttitor tempus. Vestibulum porta. Sed lobortis lorem sed mauris. Pellentesque eget augue vel pede lacinia consequat. Maecenas et nibh at neque ullamcorper mattis. Sed nec dui eget erat imperdiet placerat. Nunc sed magna. Suspendisse potenti. Aliquam vitae lorem. Morbi tristique, nunc in auctor sodales, quam risus vestibulum dui, quis consectetur diam justo ac ipsum. Mauris urna.',
      'Nulla posuere magna in magna. Phasellus egestas, nibh sed semper pulvinar, felis lorem pretium turpis, quis vestibulum urna nibh at urna. Curabitur vulputate turpis sit amet ligula. Etiam in lorem pretium tellus aliquam congue. Duis leo sem, luctus sit amet, laoreet in, viverra nec, mauris. Nullam tempus, diam ac venenatis blandit, sapien nisi feugiat orci, non sollicitudin lectus dolor vel urna. Cras enim. Suspendisse vel magna vitae leo semper accumsan. Ut laoreet nunc id augue. Praesent orci velit, dapibus sed, egestas eget, rhoncus eu, elit. Nulla nibh. Suspendisse est magna, lacinia vitae, malesuada in, tristique et, velit. Suspendisse nisi. Sed et ligula sit amet pede mollis tincidunt.',
      'Aenean tristique risus vel ligula. In hac habitasse platea dictumst. Vestibulum porttitor, urna quis scelerisque ullamcorper, urna tortor bibendum sapien, vel placerat felis dolor nec magna. Pellentesque rutrum interdum risus. Cras libero nibh, convallis a, ultrices id, fringilla id, pede. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pellentesque nibh. Vestibulum vestibulum, dui ac lacinia mollis, purus justo posuere tortor, et vehicula massa nunc nec dolor. Donec libero. Curabitur condimentum augue quis sem. Aliquam erat volutpat. Suspendisse velit. Fusce auctor dolor et lectus. Aenean consectetur.',
      'Curabitur pede tortor, blandit non, imperdiet in, vulputate in, dolor. Etiam a orci at urna posuere vulputate. Praesent et magna sed urna bibendum rhoncus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque semper sagittis sem. Ut rutrum velit nec tellus. Sed tincidunt. Vestibulum eu ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas gravida, leo non sodales aliquet, mi orci blandit mi, tristique condimentum ante magna et lacus. In pulvinar, leo ac gravida auctor, velit erat faucibus leo, non blandit dui enim in augue.',
      'Sed vitae mauris. Donec lectus. Morbi diam massa, fermentum a, ultricies non, varius nec, risus. Maecenas non massa. Vivamus at eros sollicitudin ipsum sollicitudin vehicula. Cras quam. Nunc velit dolor, hendrerit eu, mattis vitae, congue at, leo. Proin erat odio, rutrum ut, molestie sed, aliquam sed, pede. Proin molestie, mauris nec facilisis lacinia, velit eros imperdiet nunc, et pulvinar lectus lacus vitae quam. Quisque et enim vitae felis feugiat feugiat. Fusce viverra lectus at nibh. In nec velit et justo eleifend accumsan. Nunc sit amet eros. Nullam ultricies condimentum erat. Nulla massa ante, laoreet at, rhoncus nec, fermentum sit amet, odio. Donec suscipit neque euismod leo. Ut pulvinar nibh eget quam.',
      'Donec et augue. Ut sem. Sed adipiscing mi. Duis eget urna a turpis condimentum ultrices. Nam tempus adipiscing justo. Vestibulum non neque. Aliquam cursus, nisl fringilla malesuada volutpat, lorem leo consequat urna, at euismod justo sem non tellus. Donec suscipit lacus vitae tellus. Morbi mauris sapien, convallis a, luctus id, dapibus ac, orci. Nam eu ligula et elit vehicula elementum.',
      'Morbi et arcu vitae lacus lobortis blandit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc facilisis bibendum turpis. Vivamus porta turpis vitae tortor. Quisque mauris mauris, ultricies sed, condimentum sit amet, egestas nec, velit. Quisque nisi sem, imperdiet et, aliquet eget, vehicula eu, sem. Fusce est ligula, scelerisque et, cursus bibendum, ultricies et, dolor. Cras vitae nisl. Mauris dui mi, mollis ut, malesuada vel, varius dictum, lacus. Vestibulum mollis vestibulum enim. Cras aliquam, massa et tempus interdum, neque enim vulputate elit, vel ultrices odio mauris non nunc. Suspendisse sit amet ligula. Aliquam placerat lobortis enim. Vestibulum eget tellus quis tellus tincidunt convallis. Praesent vitae nibh. Quisque ornare erat quis diam. Cras eros. Donec cursus. Suspendisse vulputate quam condimentum purus.',
      'Sed pharetra augue eu neque. Phasellus dolor. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nam commodo faucibus massa. Pellentesque augue felis, blandit ut, rhoncus vel, fringilla tempor, tellus. Cras nibh. Praesent consectetur. Phasellus arcu nunc, convallis sed, feugiat ac, rhoncus viverra, tellus. Fusce dolor. Fusce in tellus. Cras viverra fringilla ligula. Nullam risus nisi, auctor egestas, ultricies quis, ultricies sagittis, odio. Maecenas porta. Cras semper nisi et est. Donec vitae pede non ante gravida faucibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      'Cras feugiat est ut purus. Morbi varius commodo odio. Nunc egestas diam a sem. Proin ac dolor. Suspendisse luctus blandit mi. Donec bibendum mauris et diam tempor aliquam. Morbi ac odio vel pede pharetra sollicitudin. Donec sapien. Nullam vestibulum. Fusce semper libero ut est. Morbi ut purus. Curabitur non nunc. Suspendisse congue. Nunc risus lacus, imperdiet eu, hendrerit vel, laoreet vel, metus. Pellentesque pulvinar nisl at nunc. Pellentesque sed eros.',
      'Maecenas sed ante. Aliquam mauris. Nunc laoreet, metus sed viverra gravida, tortor justo laoreet mi, ac sagittis justo felis a ligula. Sed faucibus dignissim magna. Curabitur sodales risus vitae sapien. Mauris facilisis diam. Phasellus neque enim, dictum eget, semper eget, pretium a, libero. Praesent augue orci, tristique at, tristique quis, auctor nec, ipsum. Sed sit amet arcu. Donec nec mi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Morbi viverra tincidunt massa. Vivamus mollis sollicitudin tellus. Etiam nisi diam, sagittis ac, aliquam vel, interdum eget, lorem. Integer congue commodo ipsum.',
      'Sed nec arcu a magna blandit fermentum. In scelerisque placerat magna. Maecenas rutrum euismod turpis. Duis consectetur. Vivamus aliquet eros et tellus. Quisque accumsan. Fusce felis tellus, euismod consectetur, hendrerit at, dapibus sit amet, est. Donec a est. Mauris sed nisi. Vivamus sed purus. In gravida diam quis nisl. Vivamus nibh elit, rutrum id, gravida eget, tempus vitae, libero.',
      'Aenean eget felis ac sem hendrerit tincidunt. Mauris magna. Aliquam tempus tempus velit. Aliquam nec magna. Aliquam orci erat, aliquet ut, aliquet eget, bibendum non, justo. Suspendisse potenti. Vivamus pharetra consectetur nibh. Maecenas fermentum ligula. Mauris consequat, augue vel facilisis molestie, leo erat imperdiet nibh, eu posuere quam eros id mi. Mauris mollis enim ornare risus.',
      'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Etiam ut metus. Aenean tincidunt odio non tortor. Etiam dictum euismod libero. In hac habitasse platea dictumst. Curabitur iaculis urna eu purus. Nulla facilisi. Donec tincidunt, nulla ac sollicitudin consectetur, nisl augue iaculis tortor, consectetur dapibus lorem tortor nec lacus. Nulla lectus est, sagittis non, egestas commodo, feugiat ut, turpis. Aliquam sem metus, euismod vel, ultrices non, pretium vel, odio. Morbi leo ligula, consequat at, tempus vitae, vestibulum id, massa. Pellentesque tincidunt, diam sit amet interdum pulvinar, neque nisi lacinia elit, a porttitor nulla lacus id lorem. Suspendisse at lorem. Praesent pede est, consectetur a, posuere tristique, ultricies eget, tortor. Ut congue. Aliquam vel ligula id nulla commodo ultricies.',
      'In a lacus accumsan nibh blandit tempor. Vestibulum orci. Integer feugiat tincidunt augue. Nulla vel odio vitae velit ullamcorper consectetur. Vivamus mattis, nisi eget iaculis auctor, est dolor faucibus dui, nec semper arcu leo lobortis erat. Proin dapibus. Sed posuere. Maecenas facilisis. Duis enim. Phasellus ac massa et leo vestibulum consectetur. Duis nisl nibh, pulvinar non, dictum id, lacinia sed, lorem. Nulla sodales. Curabitur ac nulla. Vestibulum in massa.',
      'Vivamus nec tellus quis turpis lacinia commodo. Suspendisse accumsan nibh ut metus. Pellentesque ipsum metus, rutrum a, laoreet vel, tristique at, lacus. Maecenas nec turpis vestibulum nulla scelerisque tincidunt. Morbi a mauris. Quisque quis massa. Phasellus felis risus, aliquam elementum, lobortis at, fringilla nec, nunc. Etiam dignissim tristique elit. Nullam eu orci. Quisque mattis. Nullam mollis bibendum libero. Phasellus est leo, feugiat vitae, malesuada eget, sodales et, nunc. Suspendisse imperdiet semper est. Nulla mollis purus. Donec nunc massa, elementum eu, feugiat sed, pellentesque vitae, nunc. Nulla facilisi. Donec malesuada sollicitudin risus.',
      'Nulla dui nisl, malesuada in, pretium id, lacinia vel, nibh. Ut feugiat congue odio. Sed egestas. Ut at mi id massa pellentesque posuere. Aliquam tincidunt, arcu in sodales consequat, libero massa cursus ante, ut suscipit dui quam nec augue. Nam orci. Fusce sodales interdum arcu. Praesent elit. Praesent neque nisi, viverra ac, iaculis sit amet, iaculis quis, ipsum. Nullam nibh nisl, malesuada vel, rhoncus eu, congue et, odio. Nulla ultricies ante sed pede. Donec in tortor a enim blandit vestibulum. Donec a lectus. Donec urna. Quisque dignissim leo eget erat. Ut dictum vulputate dui. Curabitur pretium dui et sem. Pellentesque imperdiet. Aliquam urna.',
      'Mauris id lectus ut dolor egestas dapibus. Sed placerat massa ut lorem. Suspendisse quis orci in nulla pretium eleifend. Suspendisse potenti. Morbi rhoncus lorem vel est. Donec eu mauris in neque euismod vehicula. Nam vitae orci. Sed condimentum diam in dui. Suspendisse laoreet mauris eu est. Donec tincidunt cursus purus.',
      'Nullam elit. Suspendisse facilisis, enim id aliquet faucibus, pede nibh pharetra est, et dictum mauris lectus a leo. Nullam viverra velit sit amet dui. Vestibulum pede. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eu lorem. Nunc libero elit, egestas eleifend, fermentum et, molestie eu, dolor. Nullam aliquam rhoncus purus. Vivamus cursus. Nullam odio neque, imperdiet ac, viverra quis, dictum in, tortor. In tincidunt magna ut metus euismod tempor. Nam consectetur nisi nec ligula. Sed vitae ligula. Cras erat ipsum, tincidunt non, laoreet ut, venenatis ac, tellus. Pellentesque nisi pede, ultrices id, ultrices ut, varius et, leo. Maecenas purus orci, gravida a, blandit in, faucibus eget, augue.',
      'Nulla neque elit, convallis sed, imperdiet et, sagittis et, eros. Curabitur eu est non pede congue tincidunt. Vivamus enim. In egestas mi ac mauris. Suspendisse augue orci, feugiat non, condimentum sit amet, aliquet et, odio. Praesent eget dolor. Sed fringilla, dui id ultricies blandit, nisl eros adipiscing sapien, nec imperdiet tellus ipsum at nulla. Nunc lectus. Praesent non orci vel urna sodales venenatis. In vel nulla nec nibh pharetra placerat. Nam et dui eu felis elementum auctor. Sed faucibus elit a enim. In commodo auctor nibh. Nam mollis. Ut dignissim nulla ac velit. Donec sit amet ante. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque molestie orci sit amet ipsum. Vivamus mollis odio quis nisi.',
      'Phasellus tristique egestas risus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In nulla. Sed ultricies metus ac purus. Donec erat justo, dapibus at, molestie in, porttitor ac, magna. Nulla facilisi. Integer non urna et diam dignissim varius. Proin tempor tortor a purus. Mauris in odio. Donec sed sem.',
      'Maecenas pulvinar varius est. Sed arcu nulla, lacinia nec, imperdiet at, sollicitudin consectetur, lacus. Integer vestibulum bibendum lacus. Vestibulum laoreet malesuada nisi. Etiam id tellus. Nam eget augue. Vivamus nisi. Ut tincidunt rutrum turpis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam euismod. Suspendisse eleifend, ligula eget sollicitudin tempor, erat augue condimentum velit, sit amet aliquet dolor eros ac diam. Quisque nibh nunc, fringilla tempor, aliquam ut, bibendum in, nibh. Fusce accumsan bibendum sapien. Nulla at metus eu orci imperdiet imperdiet. Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      'Duis feugiat, elit eget interdum tristique, velit orci tincidunt nulla, et vestibulum pede turpis eu elit. Duis diam. Duis massa turpis, semper id, sodales at, tempor fermentum, diam. Donec a massa eu ligula congue pretium. Donec mi velit, molestie ut, accumsan at, dapibus ac, dui. Aenean felis. Sed cursus. Suspendisse vitae neque. Sed malesuada velit sit amet nisl. Mauris nulla.',
      'Cras ac pede. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Duis eros risus, euismod non, rutrum vitae, mattis id, ante. Pellentesque pede orci, auctor vitae, accumsan sit amet, aliquam sodales, arcu. Duis placerat purus eu diam. Nam aliquet, nibh id feugiat ullamcorper, odio lectus porttitor est, et gravida dui elit vel magna. Sed est justo, molestie vel, rutrum ut, molestie et, turpis. In hac habitasse platea dictumst. Phasellus mauris tortor, faucibus a, aliquet in, accumsan sed, massa. Mauris condimentum, felis vel bibendum blandit, sapien mi aliquet urna, sed laoreet est tellus malesuada ipsum. Maecenas ornare, ligula mattis aliquet volutpat, magna odio tincidunt odio, et ultrices pede odio vel ipsum. Suspendisse felis mauris, malesuada et, mattis quis, tempor non, sapien. Proin condimentum, nisl ut molestie accumsan, odio justo molestie nulla, sed accumsan nibh ligula ut ligula. Morbi at nisi. Nulla consectetur. Curabitur consectetur, diam scelerisque commodo condimentum, mauris neque tempus leo, nec accumsan justo urna quis enim. In turpis. Integer quis metus. Aenean varius libero eget neque.',
      'Mauris tincidunt, purus at imperdiet congue, nunc ligula interdum lectus, eu vestibulum felis sapien et erat. Ut et diam tristique nisi placerat vulputate. Sed nisi urna, tempus ut, faucibus sed, hendrerit ac, libero. Maecenas elementum, dolor sit amet semper adipiscing, felis tellus congue eros, a ultricies risus orci ut leo. Nam aliquam condimentum felis. Praesent faucibus pede a ipsum. Fusce fringilla nulla ac risus. Nunc tincidunt. Donec fermentum, massa vel aliquet facilisis, lacus massa gravida dolor, quis tincidunt erat erat at magna. Ut interdum lectus quis massa. Donec tempor, velit id porttitor dictum, risus neque ultricies lorem, et dignissim velit lorem ut lorem. Proin pharetra, felis nec congue faucibus, est elit pretium arcu, at eleifend neque diam eu metus.',
      'Sed malesuada augue cursus metus. Praesent id leo a diam luctus lacinia. Vivamus faucibus orci sit amet ligula. Fusce eget sapien. Mauris suscipit ligula. Ut vitae lorem. Vestibulum dignissim ornare dui. Proin auctor diam a massa venenatis accumsan. Nam interdum. Vivamus elit. Integer ac lorem. Maecenas enim. Nullam tincidunt, risus a pharetra feugiat, orci est euismod libero, id blandit arcu ipsum quis arcu. Aenean fringilla odio ut tellus. Aliquam tristique mauris sed nisi.',
      'Nullam a urna ut quam vestibulum porta. Duis luctus. Vestibulum et turpis. Mauris a quam viverra dolor pellentesque lacinia. Cras fringilla ante aliquam magna dignissim dignissim. Pellentesque gravida. Vivamus lectus libero, viverra vestibulum, posuere in, ultricies at, sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus eget dui. Phasellus venenatis. Phasellus rutrum molestie libero. Vestibulum vestibulum mi eget risus. Cras faucibus nisi in elit. Aenean erat nibh, tempor vel, ullamcorper a, tempor nec, est. Quisque feugiat justo nec felis. Pellentesque porta convallis dolor.',
      'Cras nec lorem eu sapien volutpat gravida. In molestie libero vel diam. Cras nec urna eget dui fermentum lacinia. Pellentesque eget sem pretium justo pellentesque ullamcorper. Proin sit amet nisi. Etiam lacinia ultricies ligula. Nam turpis enim, elementum sit amet, porta id, imperdiet non, dui. Duis lorem sem, tincidunt vel, euismod ac, fringilla pellentesque, lorem. Integer pellentesque, eros nec viverra consectetur, erat sapien imperdiet risus, quis laoreet lacus nunc eget lacus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.',
      'Quisque in sem. Fusce sodales ullamcorper magna. Ut pulvinar ipsum vel sem. Praesent leo ipsum, viverra sit amet, faucibus vel, feugiat vel, nulla. Fusce nibh nisi, bibendum in, molestie et, hendrerit nec, eros. Morbi porttitor vehicula nisl. Vestibulum fringilla interdum leo. Etiam in risus. Maecenas nunc magna, hendrerit ut, consequat quis, ultricies ut, nulla. Mauris ullamcorper risus ut enim. Cras scelerisque arcu gravida lacus. Integer dolor. Fusce ornare sagittis eros. Etiam lobortis mattis sapien. Fusce tortor mauris, ullamcorper vitae, viverra et, vestibulum nec, magna. Vestibulum lectus. Proin cursus erat ut felis. Etiam mi lectus, ultrices eget, sollicitudin eget, lobortis et, libero. Etiam sagittis libero sit amet metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      'Pellentesque quis libero. Suspendisse porta, urna non lacinia varius, sapien augue condimentum purus, et dignissim sem urna vel lorem. Duis placerat. Mauris vel metus. Proin erat lectus, consequat non, aliquam ac, tempus a, enim. Sed a purus. Mauris metus. Vivamus feugiat. Mauris et lorem sed dolor posuere mollis. Vestibulum lobortis orci. In id eros et nulla tristique vulputate. Donec eu velit at orci posuere facilisis. Morbi nibh lacus, rhoncus ut, placerat vel, tempor eget, turpis. Nam nisl lacus, pellentesque eget, rutrum eu, commodo at, arcu.',
      'Phasellus ultrices quam vitae odio. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum sed tortor vestibulum magna dapibus congue. Cras commodo aliquam nisi. In non justo a nisi rhoncus convallis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus eget ipsum. Nunc pharetra neque egestas tortor. Fusce turpis ligula, accumsan sit amet, vehicula vitae, porttitor nec, enim. Donec dictum, odio a faucibus condimentum, lorem dui posuere tortor, vitae elementum mi neque non nibh. Integer lacinia nisl ut ipsum. Pellentesque enim purus, imperdiet id, interdum ut, aliquet vel, nunc. Vivamus dui. Cras vestibulum sollicitudin elit. Morbi nulla. Cras commodo lacus gravida risus. Maecenas a neque in lacus dapibus consequat. In pellentesque justo eget eros.',
  );
  $output = '';
  for ($i = 0; $i < $paragraphs; $i++) {
    $output .= str_replace('[lorem_ipsum]', ($i == 0 ? $prefix : '') . $lorem_ipsum[rand(0, 49)], $template);
  }
  return $output;
}